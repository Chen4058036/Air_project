package com.example.zhixinguo.air_project;

import android.app.AlertDialog;
import android.app.TimePickerDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.os.RemoteException;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.MotionEvent;
import android.view.View;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.CompoundButton;
import android.widget.PopupWindow;
import android.widget.RadioButton;
import android.widget.RadioGroup;
import android.widget.RelativeLayout;
import android.widget.Switch;
import android.widget.TextView;
import android.widget.TimePicker;
import android.widget.Toast;

import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;

/**
 * Created by chen on 2017/11/29.
 */

public class MypopWindow extends PopupWindow {

    private View view;
//    private AirConnectAPI connectAPI;
    private IBackService iBackService = null;
    private Context context;

    private Switch oacSwitch = null;
    private Switch modeSwitch = null;
    private Switch anionsw = null;
    private Switch newfansw = null;
    private Switch strongsw = null;
    private Switch disinfectsw = null;
    private CheckBox hotCheckbox = null;
    private RadioGroup fangroup = null;
    private RadioButton fanlevel0 = null;
    private RadioButton fanlevel1 = null;
    private RadioButton fanlevel2 = null;
    private RadioButton fanlevel3 = null;
    private Button cleanAlarm = null;
    private Button resetFilter = null;
    private Button setOpen = null;
    private Button setClose = null;
    private TextView closeTimer = null;
    private TextView openTimer = null;

    public MypopWindow(Context mContext,IBackService iBackService){
        this.iBackService = iBackService;
        this.context = mContext;
        this.view = LayoutInflater.from(mContext).inflate(R.layout.btns_popwin, null);

        initButtons();
        this.setOutsideTouchable(true);
        // mMenuView添加OnTouchListener监听判断获取触屏位置如果在选择框外面则销毁弹出框
        this.view.setOnTouchListener(new View.OnTouchListener() {

            public boolean onTouch(View v, MotionEvent event) {

                int height = view.findViewById(R.id.btnslayout_popwin).getTop();
                int y = (int) event.getY();
                if (event.getAction() == MotionEvent.ACTION_UP) {
                    if (y < height) {
                        dismiss();
                    }
                }
                return true;
            }
        });
        this.setContentView(this.view);
        // 设置弹出窗体的宽和高
        this.setHeight(RelativeLayout.LayoutParams.WRAP_CONTENT);
        this.setWidth(RelativeLayout.LayoutParams.MATCH_PARENT);

        // 设置弹出窗体可点击
        this.setFocusable(true);

        // 实例化一个ColorDrawable颜色为半透明
//        ColorDrawable dw = new ColorDrawable(0xb0000000);
        // 设置弹出窗体的背景
//        this.setBackgroundDrawable(dw);

        // 设置弹出窗体显示时的动画，从底部向上弹出

        this.setAnimationStyle(R.style.btns_pop_anim);
    }
    /*
   * 按钮
   * */
    private void initButtons() {
        oacSwitch = (Switch)view.findViewById(R.id.openandclosesw_popwin);
        oacSwitch.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
                Log.e("popwindow","oacswitch on checkedchanged");
                if(!buttonView.isPressed()) return;
                if (isChecked){
                    sendData(ProtocolModel.setOpenTimeProtocol(0));
                    setBtnsEnabled(true);
                }else{
                    sendData(ProtocolModel.setCloseTimeProtocol(0));
                    setBtnsEnabled(false);
                }
            }
        });

        modeSwitch = (Switch)view.findViewById(R.id.modesw_popwin);
        modeSwitch.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
                if(!buttonView.isPressed())return;
                Log.e("popwindow","modeswitch on checkedchanged");
                sendData(ProtocolModel.setModeProtocol(isChecked));
            }
        });

        anionsw = (Switch)view.findViewById(R.id.anionsw_popwin);
        anionsw.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
                if(!buttonView.isPressed())return;
                Log.e("popwindow","anionsw on checkedchanged");
                sendData(ProtocolModel.setAnionProtocol(isChecked));
            }
        });

        hotCheckbox = (CheckBox)view.findViewById(R.id.hotcheckbox_popwin);
        hotCheckbox.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
                if(!buttonView.isPressed())return;
                sendData(ProtocolModel.setHotProtocol(isChecked));
            }
        });

        newfansw = (Switch)view.findViewById(R.id.newfansw_popwin);
        newfansw.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
                if(!buttonView.isPressed())return;
                Log.e("popwindow","newFan on checkedchanged");
                sendData(ProtocolModel.setnewFanProtocol(isChecked));
                if(newfansw.isChecked()){
                    hotCheckbox.setEnabled(true);
                }else{
                    hotCheckbox.setEnabled(false);
                }
            }
        });

        strongsw = (Switch)view.findViewById(R.id.strongsw_popwin);
        strongsw.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
                if(!buttonView.isPressed())return;
                Log.e("popwindow","strongsw on checkedchanged");
                sendData(ProtocolModel.setStrongEftProtocol(isChecked));
            }
        });

        disinfectsw = (Switch)view.findViewById(R.id.disinfectsw_popwin);
        disinfectsw.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
                if(!buttonView.isPressed())return;
                Log.e("popwindow","disinfectsw on checkedchanged");
                sendData(ProtocolModel.setDisinfectProtocol(isChecked));
            }
        });

        fangroup = (RadioGroup)view.findViewById(R.id.fanlevel_popwin);
        fanlevel0 = (RadioButton)view.findViewById(R.id.fanlevel0_popwin);
        fanlevel1 = (RadioButton)view.findViewById(R.id.fanlevel1_popwin);
        fanlevel2 = (RadioButton)view.findViewById(R.id.fanlevel2_popwin);
        fanlevel3 = (RadioButton)view.findViewById(R.id.fanlevel3_popwin);
        fanlevel0.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                sendData(ProtocolModel.setFanLevelProtocol(0));
            }
        });
        fanlevel1.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                sendData(ProtocolModel.setFanLevelProtocol(1));
            }
        });
        fanlevel2.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                sendData(ProtocolModel.setFanLevelProtocol(2));
            }
        });
        fanlevel3.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                sendData(ProtocolModel.setFanLevelProtocol(3));
            }
        });

        cleanAlarm = (Button)view.findViewById(R.id.cfalarm_popwin);
        cleanAlarm.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
            sendData(ProtocolModel.cleanAlarmProtocol());
            }
        });

        resetFilter = (Button) view.findViewById(R.id.resetfilter_popwin);
        resetFilter.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                showsetFilterTime();
            }
        });

        setOpen = (Button) view.findViewById(R.id.setopen_popwin);
        setOpen.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                final int[] now = getNow();
                TimePickerDialog timePickerDialog = new TimePickerDialog(context, new TimePickerDialog.OnTimeSetListener() {
                    @Override
                    public void onTimeSet(TimePicker view, int hourOfDay, int minute) {
                        if (hourOfDay < now[0]){
                            hourOfDay += 24;
                        }
                        int hinterval = hourOfDay - now[0];
                        int minterval = minute - now[1];
                        int seconds = (hinterval*60+minterval)*60;
                        sendData(ProtocolModel.setOpenTimeProtocol(seconds));
                    }
                },now[0],now[1],true);
                timePickerDialog.show();
            }
        });

        setClose = (Button) view.findViewById(R.id.setclose_popwin);
        setClose.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                final int[] now = getNow();
                TimePickerDialog timePickerDialog = new TimePickerDialog(context, new TimePickerDialog.OnTimeSetListener() {
                    @Override
                    public void onTimeSet(TimePicker view, int hourOfDay, int minute) {
                        if (hourOfDay < now[0]){
                            hourOfDay += 24;
                        }
                        int hinterval = hourOfDay - now[0];
                        int minterval = minute - now[1];
                        int seconds = (hinterval*60+minterval)*60;
                        sendData(ProtocolModel.setCloseTimeProtocol(seconds));
                    }
                },now[0],now[1],true);
                timePickerDialog.show();
            }
        });

        openTimer = (TextView)view.findViewById(R.id.opentimer_popwin);
        closeTimer = (TextView)view.findViewById(R.id.closetimer_popwin);
    }

    void updateStatus(AirDataModel model){
        closeTimer.setText(getTimeStringBySeconds(model.closeTimer));
        openTimer.setText(getTimeStringBySeconds(model.openTimer));
        closeTimer.setText(getTimeStringBySeconds(20*60));
        openTimer.setText(getTimeStringBySeconds(20*60));
        if (model.mode.equals("关机")){
            modeSwitch.setChecked(false);
            oacSwitch.setChecked(false);
            setBtnsEnabled(false);
            return;
        }else{
            oacSwitch.setChecked(true);
        }
        setBtnsEnabled(true);
        //if (model.mode.equals("自动"))
        if (model.mode.equals("手动")){ modeSwitch.setChecked(true);}
        else{modeSwitch.setChecked(false);}

        anionsw.setChecked(model.anion);
        newfansw.setChecked(model.newFan);
        if(newfansw.isChecked()){
            hotCheckbox.setEnabled(true);
        }else{
            hotCheckbox.setEnabled(false);
        }
        hotCheckbox.setChecked(model.hot);
        strongsw.setChecked(model.strongEft);
        disinfectsw.setChecked(model.disinfect);
        if (model.fanlevel.equals("低速")) fangroup.check(R.id.fanlevel1_popwin);
        if (model.fanlevel.equals("中速")) fangroup.check(R.id.fanlevel2_popwin);
        if (model.fanlevel.equals("高速")) fangroup.check(R.id.fanlevel3_popwin);
        if (model.fanlevel.equals("关")) fangroup.check(R.id.fanlevel0_popwin);
    }

    private String getTimeStringBySeconds(int seconds){
        if (seconds <= 0){
            return "";
        }
        long time = System.currentTimeMillis() + seconds*1000;
        Date date = new Date(time);
        SimpleDateFormat simpleDateFormat = new SimpleDateFormat("HH时 mm分");
        return simpleDateFormat.format(date);
    }


    private void setBtnsEnabled(boolean enabled){
        //modeSwitch.setChecked(enabled);
        modeSwitch.setEnabled(enabled);
        anionsw.setEnabled(enabled);
        newfansw.setEnabled(enabled);
        hotCheckbox.setEnabled(enabled);
        strongsw.setEnabled(enabled);
        disinfectsw.setEnabled(enabled);
        fangroup.setEnabled(enabled);
        if (!enabled){
            disinfectsw.setChecked(false);
            strongsw.setChecked(false);
            hotCheckbox.setChecked(false);
            anionsw.setChecked(false);
            newfansw.setChecked(false);
            fangroup.check(R.id.fanlevel0_popwin);
        }
        cleanAlarm.setEnabled(enabled);
        resetFilter.setEnabled(enabled);
        fanlevel0.setEnabled(enabled);
        fanlevel1.setEnabled(enabled);
        fanlevel2.setEnabled(enabled);
        fanlevel3.setEnabled(enabled);
//        if (!enabled){
//
//        }
//        private Button setOpen = null;
//        private Button setClose = null;
    }

    private void showsetFilterTime(){
        final MyFilterView fv = new MyFilterView(context);
//        final EditText et = new EditText(context);

        new AlertDialog.Builder(context).setTitle("设置滤网时间")
                .setView(fv)
                .setPositiveButton("确定", new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int which) {
                        int num = fv.getHours();
                        if (num <= 0){
                            Toast.makeText(context,"请输入大于0的数",Toast.LENGTH_LONG).show();
                        }else{
                            sendData(ProtocolModel.resetFilterProtocol(num));
                        }
                    }
                })
                .setNegativeButton("取消", null)
                .show();
    }


    private void sendData(byte[] data){
        if (iBackService != null){
            try {
                iBackService.sendData(data);
            } catch (RemoteException e) {
                e.printStackTrace();
            }
        }
    }

    private int[] getNow(){
        long time=System.currentTimeMillis();
        final Calendar mCalendar=Calendar.getInstance();
        mCalendar.setTimeInMillis(time);
        int mHour=mCalendar.get(Calendar.HOUR_OF_DAY);
        int mMinuts=mCalendar.get(Calendar.MINUTE);
        return new int[]{mHour,mMinuts};
    }
}
