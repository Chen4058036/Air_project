package com.example.zhixinguo.air_project;

import android.content.Context;
import android.view.LayoutInflater;
import android.widget.EditText;
import android.widget.RelativeLayout;

/**
 * Created by chen on 2018/2/11.
 */

public class MyFilterView extends RelativeLayout {

    private EditText et;

    public MyFilterView(Context context) {
        super(context);

        LayoutInflater.from(context).inflate(R.layout.hour_filter_dialog, this);

        et = (EditText)findViewById(R.id.sethours_filter_dialog);
    }

    public int getHours(){
        String input = et.getText().toString();
        if (input.equals("")){
            return 0;
        }
        int num = Integer.parseInt(input);
        if (num <= 0){
            return 0;
        }
        return num;
    }
}
