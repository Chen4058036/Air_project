package com.example.zhixinguo.air_project;

import android.app.Application;

import com.tencent.bugly.crashreport.CrashReport;

/**
 * Created by chen on 2017/11/26.
 */

public class MyApplication extends Application {
    @Override
    public void onCreate() {
        super.onCreate();
//        OkSocket.initialize(this);//https://github.com/xuuhaoo/OkSocket/blob/master/README-CN.md
        CrashReport.initCrashReport(getApplicationContext(), "0da3163cc4", false);
    }
}
