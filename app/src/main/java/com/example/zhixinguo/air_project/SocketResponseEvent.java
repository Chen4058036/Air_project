package com.example.zhixinguo.air_project;

import android.util.Log;

/**
 * Created by chen on 2018/3/8.
 */

public class SocketResponseEvent {

    private static byte[] cmdNums = new byte[]{0x01,0x02,0x03,0x04,0x05,0x06,0x07,0x08,0x09,0x10,0x11,0x12,0x13,0x20};
    private static AirDataModel dataModel = null;
    private boolean responseStatus = false;

    public SocketResponseEvent(byte[] data,int length){
        if (dataModel == null){
            createDefaultData();
        }
        this.responseStatus = fenxiData(data,length);
    }

    public boolean isResponseStatus() {
        return responseStatus;
    }

    public AirDataModel getDataModel() {
        return dataModel;
    }

    private boolean fenxiData(byte[] data, int length){
        if (data[0] != 0x2a || data[1] != 0x2a){
            dataModel.errcode = 20001;
            return false;
        }
        if (data[length-2] != 0x0d || data[length-1] != 0x0a){
            dataModel.errcode = 20002;
            return false;
        }
        if (data[2] != -128){
            Log.e("guozhixin",Integer.toHexString(data[2] & 0xFF));
            dataModel.errcode = 20003;
            return false;
        }

        boolean isNoCmd = true;
        for (byte cmd:cmdNums) {
            if (cmd == data[3]){
                isNoCmd = false;
                break;
            }
        }
        if (isNoCmd){
            dataModel.errcode = 20004;
            return false;
        }
        if (data[3] == 0x11){
            dataModel.filterTime = byte2ToInt(data,6);
        }else if (data[3] == 0x20){
            int paramlength = data[4];
            byte[] v = new byte[paramlength];
            System.arraycopy(data,5,v,0,paramlength);
            dataModel.version = new String(v);
        }else{
            updateStatus(data,4);
        }
        return true;
    }

    private void updateStatus(byte[] data,int startPos){
        Log.e("guozhixin",Integer.toHexString(data[startPos] & 0xFF)+" "+Integer.toHexString(data[startPos+1] & 0xFF));
        dataModel.co2 = byte2ToInt(data,startPos+1);
        dataModel.pm = byte2ToInt(data,startPos+4);
        dataModel.voc = byte2ToInt(data,startPos+7);
        dataModel.fanlevel = getFanString(data[startPos+10]);
        dataModel.newFan = data[startPos+11]==1;
        dataModel.anion = data[startPos+12]==1;
        dataModel.disinfect = data[startPos+13]==1;
        dataModel.strongEft = data[startPos+14]==1;
        dataModel.hot = data[startPos+15]==1;
        dataModel.mode = getModeString(data[startPos+17]);
        dataModel.isAlarm = data[startPos+19]==1;
        dataModel.openTimer = bytesToInt2(data,startPos+21);
        dataModel.closeTimer = bytesToInt2(data,startPos+26);
    }

    private String getFanString(byte b){
        String a = "关";
        if (b == 1){
            a = "低速";
        }
        if (b == 2){
            a = "中速";
        }
        if (b == 3){
            a = "高速";
        }
        return a;
    }

    private String getModeString(byte b){

        String a = "";
        Log.e("getModeString","getModeString"+b);

        if(b == 0){
            a = "自动";
        }else if(b == 1){
            a = "手动";
        }else if(b == 2){
            a = "自检";
        }else if(b == 3){
            a = "关机";
        }

        return a;
    }

    private void createDefaultData(){
        dataModel.version = "v1.0";
        dataModel.fanlevel = "关";
        dataModel.mode = "关机";
        dataModel.co2 = byte2ToInt(new byte[]{0x00,(byte) 0x00},0);
        dataModel.pm = byte2ToInt(new byte[]{0x00,0x00},0);
        dataModel.voc = 0;
        dataModel.newFan = false;
        dataModel.disinfect = false;
        dataModel.anion = false;
        dataModel.strongEft = false;
        dataModel.hot = false;
        dataModel.filterTime = 300;
    }

    private int byte2ToInt(byte[] src,int offset){
        return (int) ((src[offset+1] & 0xFF) | ((src[offset] & 0xFF)<<8));
    }

    private int bytesToInt2(byte[] src, int offset) {
        int value;
        value = (int) ( ((src[offset] & 0xFF)<<24)
                |((src[offset+1] & 0xFF)<<16)
                |((src[offset+2] & 0xFF)<<8)
                |(src[offset+3] & 0xFF));
        return value;
    }
}
