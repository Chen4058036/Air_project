package com.example.zhixinguo.air_project;

import android.os.Handler;
import android.os.Message;
import android.util.Log;

import java.util.Calendar;


/**
 * Created by chen on 2017/11/11.
 */

public class AirConnectAPI extends Thread{
    private Handler handler = null;
    private SocketServer sServer = null;
    private byte[] cmdNums = new byte[]{0x00,0x01,0x02,0x03,0x04,0x05,0x06,0x07,0x08,0x09,0x10,0x11,0x12,0x13};

    private byte sendCmd = 0;
    private byte[] sendbuf = new byte[128];
    private int sendbuflen = 0;

    private AirDataModel dataModel = new AirDataModel();

    AirConnectAPI(Handler handler){
        this.handler = handler;
    }

    AirDataModel getDataModel() {
        return dataModel;
    }

    //testData
    void createDefaultData(){
        dataModel.version = "v1.0";
        dataModel.fanlevel = "关";
        dataModel.mode = "关机";
        dataModel.co2 = byte2ToInt(new byte[]{0x00,(byte) 0x00},0);
        dataModel.pm = byte2ToInt(new byte[]{0x00,0x00},0);
        dataModel.voc = 0;
        dataModel.newFan = false;
        dataModel.disinfect = false;
        dataModel.anion = false;
        dataModel.strongEft = false;
        dataModel.hot = false;
        dataModel.filterTime = 300;
    }

    void conncet(String host, int port){
        createDefaultData();
        final Handler handler = new Handler(new Handler.Callback() {
            @Override
            public boolean handleMessage(Message msg) {
                handSocketMsg(msg);
                return true;
            }
        });
        sServer = new SocketServer(handler,host,port);
        new Thread(sServer).start();

    }


    /*
    * 处理返回数据
    * */
    private void handSocketMsg(Message msg){
        if (handler == null){
            return;
        }
        if (msg.what == 0 || msg.what == 1){
            if(msg.what == 1){
                this.getAllSatus();
            }
            Message newMsg = handler.obtainMessage();
            newMsg.what = msg.what;
            newMsg.obj = msg.obj;
            handler.sendMessage(newMsg);
            return;
        }
        if (fenxiData((byte[])msg.obj,msg.arg1)){
            handler.sendEmptyMessage(100);
        }else{
            handler.sendEmptyMessage(20000);
        }

    }

    private boolean fenxiData(byte[] data, int length){
        if (data[0] != 0x2a || data[1] != 0x2a){
            dataModel.errcode = 20001;
            return false;
        }
        if (data[length-2] != 0x0d || data[length-1] != 0x0a){
            dataModel.errcode = 20002;
            return false;
        }
        if (data[2] != -128){
            Log.e("guozhixin",Integer.toHexString(data[2] & 0xFF));
            dataModel.errcode = 20003;
            return false;
        }

        boolean isNoCmd = true;
        for (byte cmd:cmdNums) {
            if (cmd == data[3]){
                isNoCmd = false;
                break;
            }
        }
        if (isNoCmd){
            dataModel.errcode = 20004;
            return false;
        }

        if (data[3] == 0x00){
            int paramlength = data[4];
            byte[] v = new byte[paramlength];
            System.arraycopy(data,5,v,0,paramlength);
            dataModel.version = new String(v);
            return true;
        }
        if (data[3] == 0x08){
            updateStatus(data,4);
            return true;
        }
        if ((data[3] >= 0x01 && data[3] <= 0x07) || data[3] == 0x12 || data[3] == 0x13){
            boolean success = data[6]==0;
            if (success){
                updateStatus(data,6);
            }
            return success;
        }
        if (data[3] == 0x09 || data[3] == 0x10){
            return data[6]==0;
        }
        if (data[3] == 0x11){
            dataModel.filterTime = byte2ToInt(data,6);
            return true;
        }
        return false;
    }

    private void updateStatus(byte[] data,int startPos){
        Log.e("guozhixin",Integer.toHexString(data[startPos] & 0xFF)+" "+Integer.toHexString(data[startPos+1] & 0xFF));
        dataModel.co2 = byte2ToInt(data,startPos+1);
        dataModel.pm = byte2ToInt(data,startPos+4);
        dataModel.voc = byte2ToInt(data,startPos+7);
        dataModel.fanlevel = getFanString(data[startPos+10]);
        dataModel.newFan = data[startPos+11]==1;
        dataModel.anion = data[startPos+12]==1;
        dataModel.disinfect = data[startPos+13]==1;
        dataModel.strongEft = data[startPos+14]==1;
        dataModel.hot = data[startPos+15]==1;
        dataModel.mode = getModeString(data[startPos+17]);
        dataModel.isAlarm = data[startPos+19]==1;
        dataModel.openTimer = bytesToInt2(data,startPos+21);
        dataModel.closeTimer = bytesToInt2(data,startPos+26);
    }

    private int byte2ToInt(byte[] src,int offset){
        return (int) ((src[offset+1] & 0xFF) | ((src[offset] & 0xFF)<<8));
    }
    private String getFanString(byte b){
        String a = "关";
        if (b == 1){
            a = "低速";
        }
        if (b == 2){
            a = "中速";
        }
        if (b == 3){
            a = "高速";
        }
        return a;
    }
    private String getModeString(byte b){

        String a = "";
        Log.e("getModeString","getModeString"+b);

        if(b == 0){
            a = "自动";
        }else if(b == 1){
            a = "手动";
        }else if(b == 2){
            a = "自检";
        }else if(b == 3){
            a = "关机";
        }

        return a;
    }

    //协议接口
    void setFanLevel(int level){
//        Message msg = handler.obtainMessage();
//        msg.what = 800;
//        msg.arg1 = this.sendCmd;
//        handler.sendMessage(msg);
        sendOneDataCmd((byte)0x01,(byte)level);}
    void setnewFan(boolean isOn){
        sendBooleanCmd((byte)0x02,isOn);
    }
    void setAnion(boolean isOn){
        sendBooleanCmd((byte)0x03,isOn);
    }
    void setDisinfect(boolean isOn){sendBooleanCmd((byte)0x04,isOn);}
    void setStrongEft(boolean isOn){
        sendBooleanCmd((byte)0x05,isOn);
    }
    void setHot(boolean isOn){
        sendBooleanCmd((byte)0x06,isOn);
    }
    void setMode(boolean isAuto){
        sendBooleanCmd((byte)0x07,isAuto);
    }
    void cleanAlarm(){sendOneDataCmd((byte)0x09,(byte) 0x55);}
    void resetFilter(int hours){
//        sendOneDataCmd((byte)0x10,(byte) 0xaa);
        if (hours < 0)
            hours = 0;
        byte[] se = intToBytes2(hours);
        if (sServer != null){
            System.arraycopy(se,2,sendbuf,0,2);
            this.sendbuflen = 2;
            Log.e("", "getAllSatus: setOpenTime");
            this.sendCmd = 0x10;
            //sServer.sendProtocolData(createProtocol((byte) 0x13,this.sendbuf,this.sendbuflen));
        }
    }
    @Override
    public void run() {
        //执行耗时操作
        Log.e("", "AirConnectAPI run");
        int num = 0;
        while(true){
            if(this.sendCmd != 0){
                if(this.sendbuflen != 0){
                    sServer.sendProtocolData(createProtocol(this.sendCmd,this.sendbuf,this.sendbuflen));
//                    Message newMsg = handler.obtainMessage();
//                    newMsg.what = 700;
//                    newMsg.arg1 = this.sendCmd;
//                    handler.sendMessage(newMsg);
                }else{
                    sServer.sendProtocolData(createProtocol(this.sendCmd,null,0));
//                    Message newMsg = handler.obtainMessage();
//                    newMsg.what = 700;
//                    newMsg.arg1 = this.sendCmd;
//                    handler.sendMessage(newMsg);
                }
                num = 0;
                this.sendCmd = 0;
            }

            if(num > 100){
                num = 0;
                this.getAllSatus();
            }else{
                num++;
            }

            try {
                Thread.sleep(100);
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
        }

    }

    private void sendBooleanCmd(byte cmd,boolean isOn){
        byte temp = (byte) (isOn?1:0);
        byte[] data = new byte[]{temp};
        if (sServer != null){
            //sServer.sendProtocolData(createProtocol(cmd,data,1));
            this.sendbuflen = 1;
            System.arraycopy(data,0,this.sendbuf,0,1);
            this.sendCmd = cmd;

        }
    }
    private void sendOneDataCmd(byte cmd,byte onedata){
        byte[] data = new byte[]{onedata};
        if (sServer != null){
            //sServer.sendProtocolData(createProtocol(cmd,data,1));
            System.arraycopy(data,0,this.sendbuf,0,1);
            this.sendbuflen = 1;
            this.sendCmd = cmd;
//            Message msg = handler.obtainMessage();
//            msg.what = 800;
//            msg.arg1 = this.sendCmd;
//            handler.sendMessage(msg);
        }
//        else {
//            Message msg = handler.obtainMessage();
//            msg.what = 900;
//            msg.arg1 = this.sendCmd;
//            handler.sendMessage(msg);
//        }

    }

    void getVersion(){
        if(sServer != null){
            //sServer.sendProtocolData(createProtocol((byte)0x00,null,0));
            this.sendbuflen = 0;
            this.sendCmd = 0x14;
        }

    }
    void getAllSatus(){
        if(sServer != null) {
            //sServer.sendProtocolData(createProtocol((byte)0x08,null,0));

            this.sendbuflen = 0;
            this.sendCmd = 0x08;
        }
    }
    void getFilterStatus(){
        if(sServer != null){
            //sServer.sendProtocolData(createProtocol((byte)0x11,null,0));

            this.sendbuflen = 0;
            this.sendCmd = 0x11;
        }

    }

    void setOpenTime(int seconds){
        if (seconds < 0) return;
        byte[] se = intToBytes2(seconds);
        if (sServer != null){
            System.arraycopy(se,0,sendbuf,0,4);
            this.sendbuflen = 4;
            Log.e("", "getAllSatus: setOpenTime");
            this.sendCmd = 0x13;
            //sServer.sendProtocolData(createProtocol((byte) 0x13,this.sendbuf,this.sendbuflen));
        }
    }
    void setCloseTime(int seconds){
        if (seconds < 0) return;
        byte[] se = intToBytes2(seconds);
        if (sServer != null){
            System.arraycopy(se,0,sendbuf,0,4);
            this.sendbuflen = 4;
            Log.e("", "getAllSatus: setCloseTime");
            this.sendCmd = 0x12;
            //sServer.sendProtocolData(createProtocol((byte) 0x12,this.sendbuf,this.sendbuflen));
        }

    }

    private byte[] createProtocol(byte cmd,byte[] data,int dlength){
        byte[] protocol = new byte[dlength+4+2+1];
        protocol[0] = 0x2a;
        protocol[1] = 0x2a;
        protocol[2] = 0x00;
        protocol[3] = cmd;
        if (dlength > 0){
            protocol[4] = (byte) dlength;
            System.arraycopy(data,0,protocol,5,dlength);
            protocol[dlength+5] = 0x0d;
            protocol[dlength+6] = 0x0a;
        }else {
            protocol[4] = 0x0d;
            protocol[5] = 0x0a;
        }
        return protocol;
    }

    int[] getNow(){
        long time=System.currentTimeMillis();
        final Calendar mCalendar=Calendar.getInstance();
        mCalendar.setTimeInMillis(time);
        int mHour=mCalendar.get(Calendar.HOUR_OF_DAY);
        int mMinuts=mCalendar.get(Calendar.MINUTE);
        return new int[]{mHour,mMinuts};
    }



    /**
     * 将int数值转换为占四个字节的byte数组，本方法适用于(低位在前，高位在后)的顺序。 和bytesToInt（）配套使用
     * @param value
     *            要转换的int值
     * @return byte数组
     */
    byte[] intToBytes( int value )
    {
        byte[] src = new byte[4];
        src[3] =  (byte) ((value>>24) & 0xFF);
        src[2] =  (byte) ((value>>16) & 0xFF);
        src[1] =  (byte) ((value>>8) & 0xFF);
        src[0] =  (byte) (value & 0xFF);
        return src;
    }
    /**
     * 将int数值转换为占四个字节的byte数组，本方法适用于(高位在前，低位在后)的顺序。  和bytesToInt2（）配套使用
     */
    byte[] intToBytes2(int value)
    {
        byte[] src = new byte[4];
        src[0] = (byte) ((value>>24) & 0xFF);
        src[1] = (byte) ((value>>16)& 0xFF);
        src[2] = (byte) ((value>>8)&0xFF);
        src[3] = (byte) (value & 0xFF);
        return src;
    }

    /**
     * byte数组中取int数值，本方法适用于(低位在前，高位在后)的顺序，和和intToBytes（）配套使用
     *
     * @param src
     *            byte数组
     * @param offset
     *            从数组的第offset位开始
     * @return int数值
     */
    int bytesToInt(byte[] src, int offset) {
        int value;
        value = (int) ((src[offset] & 0xFF)
                | ((src[offset+1] & 0xFF)<<8)
                | ((src[offset+2] & 0xFF)<<16)
                | ((src[offset+3] & 0xFF)<<24));
        return value;
    }

    /**
     * byte数组中取int数值，本方法适用于(低位在后，高位在前)的顺序。和intToBytes2（）配套使用
     */
    int bytesToInt2(byte[] src, int offset) {
        int value;
        value = (int) ( ((src[offset] & 0xFF)<<24)
                |((src[offset+1] & 0xFF)<<16)
                |((src[offset+2] & 0xFF)<<8)
                |(src[offset+3] & 0xFF));
        return value;
    }
}
