package com.example.zhixinguo.air_project;

import java.util.Date;

/**
 * Created by chen on 2017/11/11.
 */

public class AirDataModel {
    public String version;
    public String fanlevel;
    public String mode;   //0-自动  1- 手动   2- 自检   3-关机
    public Date openTime;
    public Date closeTime;

    public int co2;
    public int voc;
    public int pm;

    public boolean newFan;
    public boolean anion;
    public boolean disinfect;
    public boolean strongEft;
    public boolean hot;
    public boolean isAlarm;

    public int filterTime;

    int openTimer;
    int closeTimer;

    /*
    * errcode
    * 20001  协议头错误
    * 20002  协议尾错误
    * 20003  命令号头错误
    * 20004  命令不存在
    * */
    public int errcode;

}
