package com.example.zhixinguo.air_project;

import android.annotation.SuppressLint;
import android.app.Activity;
import android.app.AlertDialog;
import android.app.TimePickerDialog;
import android.content.ComponentName;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.ServiceConnection;
import android.content.res.Configuration;
import android.os.Bundle;
import android.os.Handler;
import android.os.IBinder;
import android.os.Message;
import android.os.PowerManager;
import android.support.v7.app.AppCompatActivity;
import android.text.Layout;
import android.util.Log;
import android.view.Gravity;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.WindowManager;
import android.widget.Button;
import android.widget.PopupWindow;
import android.widget.TextView;
import android.widget.Toast;

import com.google.zxing.integration.android.IntentIntegrator;
import com.google.zxing.integration.android.IntentResult;

import org.greenrobot.eventbus.EventBus;
import org.greenrobot.eventbus.Subscribe;
import org.greenrobot.eventbus.ThreadMode;

import java.util.Timer;
import java.util.TimerTask;

@SuppressLint("Registered")
public class MainActivity extends Activity {

    //状态栏变量
    private TextView co2status = null;
    private TextView pmstatus = null;
    private TextView vocstatus = null;
    private TextView filterStatus = null;

    private boolean isFilterTextShow = false;
    private Timer timer = null;
    private TimerTask task = null;
    private static PowerManager.WakeLock wakeLock;

    private WindowManager.LayoutParams params = null;
    private MypopWindow btnsPopWin = null;

    private IBackService iBackService = null;

    private AirDataModel dataModel = null;

    private ServiceConnection conn = new ServiceConnection() {
        @Override
        public void onServiceConnected(ComponentName componentName, IBinder iBinder) {
            iBackService = IBackService.Stub.asInterface(iBinder);
        }

        @Override
        public void onServiceDisconnected(ComponentName componentName) {
            iBackService = null;
        }
    };
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        createDefaultData();
        keepScreenOn(this,true);

        Button upBtn = (Button)findViewById(R.id.upBtn_main);
        upBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                showPopFormBottom();
            }
        });

        initStatusView();
        filterStatus.setVisibility(View.INVISIBLE);

        EventBus.getDefault().register(this);
        bindService(new Intent(MainActivity.this,
                SocketService.class),conn,BIND_AUTO_CREATE);

        updateStatus();
    }

    @Override
    protected void onDestroy() {
        stopShowFilterText();
        super.onDestroy();
        keepScreenOn(this, false);
        EventBus.getDefault().unregister(this);
    }

    //更换滤网文字闪烁
    void startShowFilterText(){
         @SuppressLint("HandlerLeak") final Handler handler = new Handler() {
            public void handleMessage(Message msg) {
                if (msg.what == 1) {
                    if (isFilterTextShow){
                        filterStatus.setVisibility(View.INVISIBLE);
                        isFilterTextShow =false;
                    }else {
                        filterStatus.setVisibility(View.VISIBLE);
                        isFilterTextShow = true;
                    }
                    super.handleMessage(msg);
                }
            };
        };
        timer = new Timer();
        task = new TimerTask() {

            @Override
            public void run() {
                // 需要做的事:发送消息
                Message message = new Message();
                message.what = 1;
                handler.sendMessage(message);
            }
        };
        timer.schedule(task,500,1000);  //第三个参数是闪烁间隔，单位是毫秒
    }
    void stopShowFilterText(){
        try{
            filterStatus.setVisibility(View.INVISIBLE);
            isFilterTextShow =false;
            timer.cancel();
            timer = null;
            task.cancel();
            task = null;
        }catch (Exception e) {
            Log.e("guozhixin","no read Buffer");
        }
    }

    public static void keepScreenOn(Context context, boolean on) {
        if (on) {
            PowerManager pm = (PowerManager) context.getSystemService(Context.POWER_SERVICE);
            wakeLock = pm.newWakeLock(PowerManager.SCREEN_BRIGHT_WAKE_LOCK | PowerManager.ON_AFTER_RELEASE, "==KeepScreenOn==");
            wakeLock.acquire();
        } else {
            if (wakeLock != null) {
                wakeLock.release();
                wakeLock = null;
            }
        }
    }

    /*
    * 状态栏
    * */
    void initStatusView(){
        co2status = (TextView)findViewById(R.id.co2status_main);
        pmstatus = (TextView)findViewById(R.id.pmstatus_main);
        vocstatus = (TextView)findViewById(R.id.vocstatus_main);
        filterStatus = (TextView)findViewById(R.id.filterstatus_main);
    }
    private void updateStatus(){
        if(dataModel.pm != 0xffff){
            pmstatus.setText(String.valueOf(dataModel.pm));
        }
        if(dataModel.co2 != 0xffff){
            co2status.setText(new StringBuilder().append(dataModel.co2).append(" ppm").toString());
        }
        if(dataModel.voc != 0xffff){
            vocstatus.setText(new StringBuilder().append(dataModel.voc).append(" 级").toString());
        }
        if(dataModel.isAlarm){
            this.startShowFilterText();
        }else{
            this.stopShowFilterText();
        }

//        filterstatus.setText("滤网剩余时间: "+model.filterTime +" 小时");
//        version.setText("固件版本: "+model.version);
    }


    /*
    * 扫码流程
    * */
    @Override
    public boolean onCreateOptionsMenu(Menu menu){
        MenuInflater inflater = getMenuInflater();
        inflater.inflate(R.menu.menu_actionbar, menu);
        return super.onCreateOptionsMenu(menu);
    }
    @Override
    public boolean onOptionsItemSelected(MenuItem item) {

        switch (item.getItemId()) {
            case R.id.action_connect:
//                connectAPI.conncet("192.168.0.88",8080);
                //connectAPI.conncet("192.168.3.71",8080);
                return true;
            case R.id.action_scanQR:
                Log.d("scanQR","进入扫码");
                goToScan();
                return true;
            default:
                return super.onOptionsItemSelected(item);
        }
    }

    public void showPopFormBottom() {
        if (btnsPopWin == null) {
            btnsPopWin = new MypopWindow(this, iBackService);
        }
//        设置Popupwindow显示位置（从底部弹出）
        btnsPopWin.showAtLocation(findViewById(R.id.mainview_main), Gravity.BOTTOM | Gravity.CENTER_HORIZONTAL, 0, 0);
        params = getWindow().getAttributes();
        //当弹出Popupwindow时，背景变半透明
        params.alpha = 0.7f;
        getWindow().setAttributes(params);
        //设置Popupwindow关闭监听，当Popupwindow关闭，背景恢复1f
        btnsPopWin.setOnDismissListener(new PopupWindow.OnDismissListener() {
            @Override
            public void onDismiss() {
                params = getWindow().getAttributes();
                params.alpha = 1f;
                getWindow().setAttributes(params);
            }
        });
    }
    void updatePopWinStatus(){
        if (btnsPopWin == null) {
            btnsPopWin = new MypopWindow(this, iBackService);
        }
        btnsPopWin.updateStatus(dataModel);
    }
    void goToScan() {
        new IntentIntegrator(this).setOrientationLocked(false).setCaptureActivity(ScanQRActivity.class).initiateScan();
    }
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        IntentResult intentResult = IntentIntegrator.parseActivityResult(requestCode,resultCode,data);
        if(intentResult != null) {
            if(intentResult.getContents() == null) {
                Toast.makeText(this,"内容为空",Toast.LENGTH_LONG).show();
            } else {
                String ScanResult = intentResult.getContents();
                showDialogMsg("扫描成功:"+ScanResult);
//                Toast.makeText(this,"扫描成功:"+ScanResult,Toast.LENGTH_LONG).show();
                // ScanResult 为 获取到的字符串
            }
//            connectAPI.conncet("192.168.1.100",10003);
        } else {
            super.onActivityResult(requestCode,resultCode,data);
        }
    }
    public void showDialogMsg(String msg) {
        new AlertDialog.Builder(this).setTitle("notification").setMessage(msg)
                .setPositiveButton("ok", new DialogInterface.OnClickListener() {

                    @Override
                    public void onClick(DialogInterface dialog, int which) {

                    }
                }).show();
    }

    @Subscribe(threadMode = ThreadMode.MAIN)
    public void handleSocketEvent(SocketResponseEvent event) {
        Log.e("eventBus", "handleSocketEvent: ");
        dataModel = event.getDataModel();
        updateStatus();
        updatePopWinStatus();
    }

    void createDefaultData(){
        if (dataModel == null){
            dataModel = new AirDataModel();
        }
        dataModel.version = "v1.0";
        dataModel.fanlevel = "关";
        dataModel.mode = "关机";
        dataModel.co2 = byte2ToInt(new byte[]{0x00,(byte) 0x00},0);
        dataModel.pm = byte2ToInt(new byte[]{0x00,0x00},0);
        dataModel.voc = 0;
        dataModel.newFan = false;
        dataModel.disinfect = false;
        dataModel.anion = false;
        dataModel.strongEft = false;
        dataModel.hot = false;
        dataModel.filterTime = 300;
    }

    private int byte2ToInt(byte[] src,int offset){
        return (int) ((src[offset+1] & 0xFF) | ((src[offset] & 0xFF)<<8));
    }

    //生成二维码的代码
//    String qrTest = "thisisqrteststring";
//    Bitmap bmp = this.encodeAsBitmap(qrTest);
//    Bitmap encodeAsBitmap(String str){
//        Bitmap bitmap = null;
//        BitMatrix result = null;
//        MultiFormatWriter multiFormatWriter = new MultiFormatWriter();
//        try {
//            result = multiFormatWriter.encode(str, BarcodeFormat.QR_CODE, 800, 1000);
//            // 使用 ZXing Android Embedded 要写的代码
//            BarcodeEncoder barcodeEncoder = new BarcodeEncoder();
//            bitmap = barcodeEncoder.createBitmap(result);
//        } catch (WriterException e){
//            e.printStackTrace();
//        } catch (IllegalArgumentException iae){ // ?
//            return null;
//        }
//
//        // 如果不使用 ZXing Android Embedded 的话，要写的代码
//
////        int w = result.getWidth();
////        int h = result.getHeight();
////        int[] pixels = new int[w * h];
////        for (int y = 0; y < h; y++) {
////            int offset = y * w;
////            for (int x = 0; x < w; x++) {
////                pixels[offset + x] = result.get(x, y) ? BLACK : WHITE;
////            }
////        }
////        bitmap = Bitmap.createBitmap(w,h,Bitmap.Config.ARGB_8888);
////        bitmap.setPixels(pixels,0,100,0,0,w,h);
//
//        return bitmap;
//    }
}
