package com.example.zhixinguo.air_project;

import android.os.Handler;
import android.os.Message;
import android.util.Log;

import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.net.Socket;

class SocketServer implements Runnable{
    private Socket socket = null;
    private InputStream in = null;
    private OutputStream out = null;
    private Handler mHandler;
    private String host = "192.168.0.88";//"192.168.0.103";
    private int port = 8080;//9000;
    private boolean stopFlag = false;

    SocketServer(Handler mHandler,String host,int port){
        this.mHandler = mHandler;
        this.host = host;
        this.port = port;
    }

    void sendProtocolData(byte[] data){
        if (socket.isConnected()) {
            if (!socket.isOutputShutdown()) {
                try {
                    out.write(data);
//                    out.flush();
                } catch (IOException e) {
                    e.printStackTrace();
                }
            }
        }
    }

    public void setStopFlag(boolean stopFlag) {
        this.stopFlag = stopFlag;
    }

    /**
     * 读取服务器发来的信息，并通过Handler发给UI线程
     */

    private static String byte2hex(byte [] buffer){
        String h = "";

        for(int i = 0; i < buffer.length; i++){
            String temp = Integer.toHexString(buffer[i] & 0xFF);
            if(temp.length() == 1){
                temp = "0" + temp;
            }
            h = h + " "+ temp;
        }

        return h;

    }

    @Override
    public void run() {
        try {
            socket = new Socket(host, port);
            in = socket.getInputStream();
            out = socket.getOutputStream();
            mHandler.sendEmptyMessage(1);
            //socket.setSoTimeout(3000);
            byte[] buffer = new byte[100];
            int nowWei = 0;
            while (true) {
                if (!socket.isClosed()) {
                    if (socket.isConnected()) {
                        if (!socket.isInputShutdown()) {
                            try {
                                int length = in.read(buffer);
                                if (length > 0) {
                                    Log.e("guozhixin","read Buffer"+byte2hex(buffer));
                                    nowWei = 0;
                                    while(true){
                                        if(length - nowWei > 0){
                                            Log.e("guozhixin","read length "+Integer.toString(length - nowWei));
                                            byte[] data = new byte[length - nowWei];
                                            System.arraycopy(buffer,nowWei,data,0,length - nowWei);
                                            Message msg = mHandler.obtainMessage();
                                            msg.what = 100;
                                            msg.obj = data;
                                            msg.arg1 = length - nowWei;
                                            mHandler.sendMessage(msg);
                                            nowWei = nowWei + 26;
                                        }else{
                                            break;
                                        }
                                    }

                                }else{
                                    socket = new Socket(host,port);
                                    in = socket.getInputStream();
                                    out = socket.getOutputStream();
                                    Log.e("guozhixin","connecting !!!");
                                }
                            }
                            catch (Exception e) {
                                Log.e("guozhixin","no read Buffer");
                                socket = new Socket(host,port);
                                in = socket.getInputStream();
                                out = socket.getOutputStream();
                                Log.e("guozhixin","connecting !!!");
                            }
                        }
                    }
                }

                if (stopFlag){
                    break;
                }
            }
        } catch (Exception e) {
            e.printStackTrace();
            Message msg = mHandler.obtainMessage();
            msg.what = 0;
            msg.obj = e.toString();
            mHandler.sendMessage(msg);
        }
    }
}
