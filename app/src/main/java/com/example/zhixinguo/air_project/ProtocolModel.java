package com.example.zhixinguo.air_project;

/**
 * Created by chen on 2018/3/8.
 */

public class ProtocolModel {

    static byte[] getAllSatusProtocol(){
        return createProtocol((byte)0x08,null,0);
    }

    static byte[] setOpenTimeProtocol(int seconds) {
        byte[] se = intToBytes2(seconds);
        return createProtocol((byte) 0x13,se,4);
    }

    static byte[] setCloseTimeProtocol(int seconds){
        byte[] se = intToBytes2(seconds);
        return createProtocol((byte) 0x12,se,4);
    }

    static byte[] setModeProtocol(boolean isAuto){
        byte temp = (byte) (isAuto?1:0);
        byte[] data = new byte[]{temp};
        return createProtocol((byte) 0x07,data,1);
    }

    static byte[] setHotProtocol(boolean isOn){
        byte temp = (byte) (isOn?1:0);
        byte[] data = new byte[]{temp};
        return createProtocol((byte) 0x06,data,1);
    }

    static byte[] setAnionProtocol(boolean isOn){
        byte temp = (byte) (isOn?1:0);
        byte[] data = new byte[]{temp};
        return createProtocol((byte) 0x03,data,1);
    }

    static byte[] setnewFanProtocol(boolean isOn){
        byte temp = (byte) (isOn?1:0);
        byte[] data = new byte[]{temp};
        return createProtocol((byte) 0x02,data,1);
    }

    static byte[] setStrongEftProtocol(boolean isOn){
        byte temp = (byte) (isOn?1:0);
        byte[] data = new byte[]{temp};
        return createProtocol((byte) 0x05,data,1);
    }

    static byte[] setDisinfectProtocol(boolean isOn){
        byte temp = (byte) (isOn?1:0);
        byte[] data = new byte[]{temp};
        return createProtocol((byte) 0x04,data,1);
    }

    static byte[] setFanLevelProtocol(int level){
        byte[] data = new byte[]{(byte) level};
        return createProtocol((byte) 0x01,data,1);
    }

    static byte[] cleanAlarmProtocol(){
        byte[] data = new byte[]{(byte) 0x55};
        return createProtocol((byte) 0x09,data,1);
    }

    static byte[] resetFilterProtocol(int hours){
        if (hours < 0)
            hours = 0;
        byte[] se = intToBytes2(hours);
        return createProtocol((byte) 0x10,se,4);
    }

    private static byte[] createProtocol(byte cmd,byte[] data,int dlength){
        byte[] protocol = new byte[dlength+4+2+1];
        protocol[0] = 0x2a;
        protocol[1] = 0x2a;
        protocol[2] = 0x00;
        protocol[3] = cmd;
        if (dlength > 0){
            protocol[4] = (byte) dlength;
            System.arraycopy(data,0,protocol,5,dlength);
            protocol[dlength+5] = 0x0d;
            protocol[dlength+6] = 0x0a;
        }else {
            protocol[4] = 0x0d;
            protocol[5] = 0x0a;
        }
        return protocol;
    }

    private static byte[] intToBytes2(int value)
    {
        byte[] src = new byte[4];
        src[0] = (byte) ((value>>24) & 0xFF);
        src[1] = (byte) ((value>>16)& 0xFF);
        src[2] = (byte) ((value>>8)&0xFF);
        src[3] = (byte) (value & 0xFF);
        return src;
    }
}
